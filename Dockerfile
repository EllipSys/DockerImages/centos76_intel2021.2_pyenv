FROM registry.windenergy.dtu.dk/ellipsys/dockerimages/centos76_intel2021.2

RUN yum install -y java-1.8.0-openjdk

WORKDIR /home/gitlab-runner

RUN yum install -y patch
ENV PYENV_ROOT /home/gitlab-runner/.pyenv
ENV PATH /usr/local/openssl/bin:${PYENV_ROOT}/bin:${PATH}
ENV CONFIGURE_OPTS -with-openssl=/usr/local/openssl
ENV CPPFLAGS=-I/usr/local/openssl/include
ENV LDFLAGS=-L/usr/local/openssl/lib
ENV LD_LIBRARY_PATH /usr/local/openssl/lib:${LD_LIBRARY_PATH}
RUN pyenv install -s 3.7.16 && \
pyenv install -s 3.8.16 && \
pyenv install -s 3.9.16 && \
pyenv install -v -s 3.10.10  && \
pyenv install -v -s 3.11.2

