# centos76_intel2021.2_pyenv

Docker image based on Intel OneAPI 2021.2 with pyenv, with the following pre-installed python versions:

* 3.7.16
* 3.8.16
* 3.9.16
* 3.10.10
* 3.11.2

